var called = 1
var occupiedMasseurs = []
var freeMasseurs = []
var j = 50
var currentMissions = []

function compare(a, b) {
  var comparison = 0;
  if (a.start > b.start) {
    comparison = 1;
  } else if (a.start < b.start) {
    comparison = -1;
  }
  return comparison;
}

function calculDistance(x1, x2, y1, y2) {
  var d = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2))
  return d
}

function attributeMissions(missions) {
  var result = {}
  var n = missions.length
  if (j - n > 0) {
    for (var i = 0; i < n; i++) {
      result[i + 1 + 50 - j] = {}
      result[i + 1 + 50 - j].x = missions[i].x
      result[i + 1 + 50 - j].y = missions[i].y
      occupiedMasseurs.push({
        'num': i + 1 + 50 - j,
        'x': missions[i].x,
        'y': missions[i].y,
        'temps': missions[i].start + missions[i].length
      })
      var mission = missions[i]
      mission.executed = true
      mission.masseur = i + 1 + 50 - j
      currentMissions.push(mission)
    }
    j = j - n
  } else {
      freeMasseurs = occupiedMasseurs.filter(function (masseur) {
      return masseur.temps <= called * 5;
    });
      currentMissions = currentMissions.filter(function (mission) {
      return mission.start + mission.length >= called * 5
    })
    if(currentMissions.length+n<=50){
      currentMissions = currentMissions.concat(missions)
    }

    currentMissions.sort(compare)
    console.log(currentMissions)

    for (var l = 0; l < currentMissions.length - 1; l++) {
      if (!currentMissions[l].executed) {

        if (freeMasseurs.length > 0) {
          var indice = 0
          var occupied = false
          var occupiedIndice = 0
          var d = calculDistance(currentMissions[l].x, freeMasseurs[0].x, currentMissions[l].y, freeMasseurs[0].y)
          var t = d / 0.25
          for (var f = 1; f < freeMasseurs.length; f++) {
            if (d > calculDistance(currentMissions[l].x, freeMasseurs[f].x, currentMissions[l].y, freeMasseurs[f].y)) {
              indice = f
              d = calculDistance(currentMissions[l].x, freeMasseurs[f].x, currentMissions[l].y, freeMasseurs[f].y)
              t = d / 0.25
            }
          }
          result[freeMasseurs[indice].num] = {}
          result[freeMasseurs[indice].num].x = currentMissions[l].x
          result[freeMasseurs[indice].num].y = currentMissions[l].y
          freeMasseurs[indice].temps = currentMissions[l].start + currentMissions[l].length
          occupiedMasseurs.push(freeMasseurs[indice])
          currentMissions[l].executed = true
          currentMissions[l].masseur=freeMasseurs[indice].num
          freeMasseurs.splice(indice, 1)
        }
       else {
          for (var k = currentMissions.length - 1; k > l; k--) {
            if (currentMissions[k].executed == true ) {
              for(var w=0;w<occupiedMasseurs.length-1;w++){
              if(occupiedMasseurs[w].num==currentMissions[k].masseur){
                if(!occupiedMasseurs[w].switched){
                  currentMissions[l].masseur=currentMissions[k].masseur
                  result[currentMissions[k].masseur] = {}
                  result[currentMissions[k].masseur].x = currentMissions[l].x
                  result[currentMissions[k].masseur].y = currentMissions[l].y
                  currentMissions[l].executed = true
                  currentMissions[k].executed = false
                  occupiedMasseurs[w].temps=currentMissions[l].start+currentMissions[l].length
                  occupiedMasseurs[w].switched=true
                }
               }
              }
              break
            }

          }
        }
      }
    }
  }
    called++
    return result
}
