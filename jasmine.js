const waitingMasseurs   = [];
const doingMasseurs     = [];
const instructions = {};
	init();

function calculateLength (startX, startY, endX, endY) {
	const x = endX - startX;
	const y = endY - startY;

	return Math.sqrt(x * x + y * y);
}

function calculateDuration (length) {
	return length * 55 / 60;
}

function init () {
	let i = 0;

	while (50 > i++) {
		const masseur = {
			id: i,
			availableTime: 0,
			availableLocationX: 0,
			availableLocationY: 0,
		}
	waitingMasseurs.push(masseur);
	}
	console.log(waitingMasseurs);
}

function attributeMissions (missionArray) {

	missionArray.forEach((mission) => {
		if (!waitingMasseurs.length) return;

		const curMission            = mission;
		const arrcurMasseur         = waitingMasseurs.splice(0, 1);
		const curMasseur            = arrcurMasseur[0];
		curMasseur.availableTime        = curMission.start;
		curMasseur.availableTime        += curMission.start;
		curMasseur.availableTime        += curMission.length;
		let length                      = calculateLength(curMasseur.availableLocationX, curMasseur.availableLocationY, curMission.x, curMission.y);
		curMasseur.availableTime        += calculateDuration(length);
		curMasseur.availableLocationX   += curMission.x;
		curMasseur.availableLocationY   += curMission.y;

		doingMasseurs.push(curMasseur);

		instructions[curMasseur.id] = {
			x: curMasseur.availableLocationX,
			y: curMasseur.availableLocationY,
		};

});

console.log(instructions);
return instructions;
}
