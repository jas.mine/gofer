var formatLocalDate = require('../../formatLocalDate');
var database = require('../../database');
var cities;
var stations = [];
var processType = "CDC";
var company = "Global";
var counter = 0;
var step = 0;

checkStationNameMatchCityName();

function checkStationNameMatchCityName(){
  database.getPool().query('SELECT s.short_name,c.name FROM station s JOIN city as c on c.id = s.city_id WHERE c.name <> s.short_name', function(err, rows, fields) {
    counter = rows.length;
   for (var i = 0; i < rows.length;i++){
    report("City name "+rows[i]["name"]+" doesn't match station short_name "+rows[i]["short_name"],1)
   }
   if (rows.length==0){next();}
  });
}

function checkConvertStationIdWithoutStation(){
  database.getPool().query('SELECT * FROM convert_station_id LEFT JOIN station ON station.id = convert_station_id.sobus_station_id WHERE station.id IS NULL', function(err, rows, fields) {
    if (err)console.log(err.stack)
    counter = rows.length;
   for (var i = 0; i < rows.length;i++){
    report("Convert Station Id "+rows[i]["sobus_station_id"]+" has no station",1)
   }
   if (rows.length==0){next();}
  });
}

function checkStationIdWithoutConvertStationId(){
  database.getPool().query('SELECT * FROM station LEFT JOIN convert_station_id ON convert_station_id.sobus_station_id = station.id WHERE convert_station_id.sobus_station_id IS NULL', function(err, rows, fields) {
    if (err)console.log(err.stack)
    counter = rows.length;
   for (var i = 0; i < rows.length;i++){
    report("Station Id "+rows[i]["id"]+" "+rows[i]["short_name"]+" "+rows[i]["long_name"]+" has no convert station id",1)
   }
   if (rows.length==0){next();}
  });
}

function checkStationIdWithoutCity(){
  database.getPool().query('SELECT s.* FROM station s LEFT JOIN city ON city.id = s.city_id WHERE city.id IS NULL', function(err, rows, fields) {
    if (err)console.log(err.stack)
    counter = rows.length;
   for (var i = 0; i < rows.length;i++){
    report("Station "+rows[i]["id"]+" "+rows[i]["short_name"]+" "+rows[i]["long_name"]+" is linked to a city that doesnt exits : "+rows[i]["city_id"],1)
   }
   if (rows.length==0){next();}
  });
}

function checkStationWithoutAnyTicket(){
  database.getPool().query('SELECT * FROM station LEFT JOIN tickets_normalized AS t ON t.departure_id= station.id LEFT JOIN tickets_normalized AS t2 ON t2.arrival_id= station.id WHERE t.departure_id IS NULL AND t2.arrival_id IS NULL', function(err, rows, fields) {
    if (err)console.log(err.stack)
    counter = 1;
    report("Database contains "+rows.length+" stations without any ticket saved linked to.",0);
  });
}

function CheckStationAreNotTooFarFromCity(){
  database.getPool().query('SELECT s.id,s.short_name,s.long_name,s.latitude,s.longitude,c.latitude as city_latitude, c.longitude as city_longitude FROM station s JOIN city AS c ON c.id = s.city_id', function(err, rows, fields) {
    if (err)console.log(err.stack)
    counter = rows.length;
   for (var i = 0; i < rows.length;i++){
    if ((rows[i]["latitude"]!=0)&&(rows[i]["longitude"]!=0)&&(computeDistance(rows[i]["latitude"],rows[i]["longitude"],rows[i]["city_latitude"],rows[i]["city_longitude"]) > 22000)){
      report("Station "+rows[i]["id"]+" "+rows[i]["short_name"]+" "+rows[i]["long_name"]+" seems too far from the city : "+computeDistance(rows[i]["latitude"],rows[i]["longitude"],rows[i]["city_latitude"],rows[i]["city_longitude"])+"m",1)
    } else {
      counter--;
    }
   }
   if (rows.length==0){next();}
  });
}

function CheckStationAreNotTooCloseFromOtherStation(){
  // using 2 loops to check an element to the rest of array
  database.getPool().query('select s.longitude, s.latitude from station s order by longitude ASC, latitude DESC', function(err, rows, fields){
    if (err) console.log(err)
      counter = rows.length;
    for (var i = 0; i < rows.length; i++){
      for (var k = i+1 ; k< rows.length; k++){
        rows[i]['distance'] = computeDistance(rows[i]['latitude'], rows[i].['longitude'], rows[k]['latitude'], rows[k]['longitude']);
        if (rows[i]['distance'] < 20) rows[i].remove();
      }
      if (rows.length==0){next();}
    }
  }
}

function CheckConsistencyBetweenExternalAndInternalOuibusAPI(){
  counter = 1000000;
  database.getPool().query('SELECT * FROM `tickets_normalized` t JOIN convert_station_id AS c ON c.sobus_station_id = t.departure_id JOIN convert_station_id AS c2 ON c2.sobus_station_id = t.arrival_id WHERE t.company_id = 3 AND (c.ouibus_intern_station_id IS NULL OR c2.ouibus_intern_station_id IS NULL)', function(err, rows, fields) {
    if (err)console.log(err.stack)
    if (rows.length != 0){
      report(rows.length+" tickets are in DB but has no internal Ouibus ID (unbookable). Please run SELECT * FROM tickets_normalized t JOIN convert_station_id AS c ON c.sobus_station_id = t.departure_id JOIN convert_station_id AS c2 ON c2.sobus_station_id = t.arrival_id WHERE t.company_id = 3 AND (c.ouibus_intern_station_id IS NULL OR c2.ouibus_intern_station_id IS NULL)",2);
    }
      database.getPool().query('SELECT t.departure_id,s.short_name,s.long_name FROM `tickets_normalized` t JOIN convert_station_id AS c ON c.sobus_station_id = t.departure_id JOIN station AS s ON s.id = t.departure_id WHERE (t.company_id = 5 OR t.company_id = 3) AND c.ouibus_intern_station_id IS NULL GROUP BY t.departure_id', function(err, rows2, fields) {
        if (err)console.log(err.stack)
        for (var i = 0; i < rows2.length; i++){
          report("Station "+rows2[i]["departure_id"]+" "+rows2[i]["short_name"]+" "+rows2[i]["long_name"]+" has ouibus tickets in DB but no internal ouibus ID (tickets unbookable).",2)
        }
          database.getPool().query('SELECT * FROM `convert_station_id` c JOIN convert_station_id AS c2 ON c2.ouibus_station_id = c.ouibus_station_id WHERE c2.sobus_station_id <> c.sobus_station_id AND c2.ouibus_station_id <> 0', function(err, rows3, fields) {
            if (err)console.log(err.stack)
            if (rows3.length != 0){
              report("2 sobus stations has the same external Ouibus Id.Please run SELECT * FROM convert_station_id c JOIN convert_station_id AS c2 ON c2.ouibus_station_id = c.ouibus_station_id WHERE c2.sobus_station_id <> c.sobus_station_id AND c2.ouibus_station_id <> 0")
            }
              database.getPool().query('SELECT * FROM `convert_station_id` c JOIN convert_station_id AS c2 ON c2.ouibus_intern_station_id = c.ouibus_intern_station_id WHERE c2.sobus_station_id <> c.sobus_station_id AND c2.ouibus_intern_station_id IS NOT NULL', function(err, rows4, fields) {
                if (err)console.log(err.stack)
                if (rows4.length != 0){
                  report("2 sobus stations has the same internal Ouibus Id.Please run SELECT * FROM convert_station_id c JOIN convert_station_id AS c2 ON c2.ouibus_intern_station_id = c.ouibus_intern_station_id WHERE c2.sobus_station_id <> c.sobus_station_id AND c2.ouibus_intern_station_id IS NOT NULL")
                }
                setTimeout(function () {next()}, 1000);
              });
          });
      });
  });
}

function computeDistance(depLat, depLong,arrLat,arrLong){
    const earthRadiusKm = 6371;
    const radDepLat = Math.PI * depLat/180;
    const radArrLat = Math.PI * arrLat/180;
    const dLat      = Math.PI * (arrLat-depLat)/180;
    const dLng      = Math.PI * (arrLong-depLong)/180;


    const a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLng/2) * Math.sin(dLng/2) * Math.cos(radDepLat) * Math.cos(radArrLat);

    const c = 2*Math.atan(Math.sqrt(a), Math.sqrt(1-a));
    return earthRadiusKm * c;
}

function report(message,loglevel){
  console.log(message);
  database.getPool().query('INSERT INTO update_reports (date, company, report, log_level, process) VALUES ("'+
    formatLocalDate.now()+'","'+company+'","'+message+'",'+loglevel+',"'+processType+'")', function(err, rows, fields) {
      //console.log(err);
      counter--;
      if (counter<=0){
        next();
      }
      });
}
function next(){
  if (step == 0){
    checkConvertStationIdWithoutStation();
  }
  if (step == 1){
    checkStationIdWithoutConvertStationId();
  }
  if (step == 2){
    checkStationIdWithoutCity();
  }
  if (step == 3){
    checkStationWithoutAnyTicket();
  }
  if (step == 4){
    CheckStationAreNotTooFarFromCity();
  }
  if (step == 5){
    CheckStationAreNotTooCloseFromOtherStation();
  }
  if (step == 6){
    CheckConsistencyBetweenExternalAndInternalOuibusAPI();
  }
  if (step == 7){
    counter = 1;
    report("End : SUCCESS",0);
  }
  if (step == 8){
    process.exit();
  }
  step++;
}

process.on('uncaughtException', function (err) {
  report("CRASHED : "+err.stack,2,true);
});
